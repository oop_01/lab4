import java.util.Scanner;

public class Array7 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        int sum = 0;
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int j = 0; j < arr.length; j++) {
            System.out.print(arr[j] + " ");
            sum = sum + arr[j];

        }
        System.out.print("\nsum = " + sum);
        double avg = ((double) sum) / arr.length;
        System.out.println("\navg = " + avg);
        int index = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[index] > arr[i]) {
                index = i;
            }
        }
        System.out.println("min = "+ arr[index] +" index = "+ index);
    }

}
