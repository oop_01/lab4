import java.util.Scanner;

public class Array4 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int j = 2; j <= arr.length && j >= 0; j--) {
            System.out.print(arr[j] + " ");
        }
    }
}
