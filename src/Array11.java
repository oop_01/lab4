import java.util.Scanner;

public class Array11 {
    public static void main(String[] args) {
        int arr[] = new int[5];
        int first, second;
        Scanner sc = new Scanner(System.in);
        System.out.print("Array\n");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random()*100);
        }
        while (true) {
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
            System.out.print("Please input index: ");
            first = sc.nextInt();
            second = sc.nextInt();
    
            // Swap
            int temp = arr[first];
            arr[first] = arr[second];
            arr[second] = temp;
            
            boolean isFinish = true ;
            for (int j = 0; j < arr.length-1; j++) {
                if(arr[j] > arr[j+1]){
                    isFinish = false;
                }
            }
            if (isFinish) {
                for (int i = 0; i < arr.length; i++) {
                    System.out.print(arr[i] + " ");
                }
                System.out.println();
                System.out.print("You win!!!");
                break;
            }
        }
    }
}
