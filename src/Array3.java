import java.util.Scanner;

public class Array3 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int j = 0; j < arr.length; j++) {
            System.out.print(arr[j]+" ");
        }
    }

}
